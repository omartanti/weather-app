const request = require('request');

let getWeather = (lat, long, callback) => {
    request({
        url: `https://api.darksky.net/forecast/efb16dde4e0805c2ff6c44b6dc417e38/${lat},${long}`,
        json: true
    }, (error, response, body) => {
        if (error) {
            callback("Unable to connect to forecast.io servers", []);
            console.log();
        } else if (response.statusCode !== 200) {
            callback("Unable to fetch weather", []);
        } else {
            callback(null, {
                temperature: body.currently.temperature,
                apparentTemperature: body.currently.apparentTemperature,
            });
        }
    });
};

module.exports = {
  getWeather
};