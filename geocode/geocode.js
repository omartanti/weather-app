const request = require('request');

let geocodeAddress = (address, callback) => {
    let addressEncoded = encodeURIComponent(address);

    request({
        url: `http://www.mapquestapi.com/geocoding/v1/address?key=qqgefncuq0uIRtBxINoPgRhHEX2bhfH2&location=${addressEncoded}`,
        json: true
    }, (error, response, body) => {

        if (error) {
            callback('Unable to connect to Maps Servers', []);
        } else if (body.info.statuscode !== 0) {
            callback('Could not find address location', []);
        } else {
            // console.log(JSON.stringify(body, undefined, 2));
            callback(null, {
                street: body.results[0].locations[0].street,
                county: body.results[0].locations[0].adminArea4,
                city: body.results[0].locations[0].adminArea5,
                country: body.results[0].locations[0].adminArea1,
                lat: body.results[0].locations[0].latLng.lat,
                long: body.results[0].locations[0].latLng.lng,
            });
        }

    });
};

module.exports = {
    geocodeAddress
};