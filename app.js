const yargs = require('yargs');

const geocode = require('./geocode/geocode');
const weather = require('./weather/weather');

const argv = yargs
    .options({
        address: {
            alias: 'a',
            demand: true,
            describe: 'Address to fetch weather for',
            string: true,
        }
    })
    .help()
    .alias('help', 'h') //this creates an alias for help as 'h'
    .argv;

geocode.geocodeAddress(argv.address, (errorMsg, geoResults) => {
    if (errorMsg) {
        console.log(errorMsg);
    } else {
        console.log(JSON.stringify(geoResults, undefined, 2));
        weather.getWeather(geoResults.lat, geoResults.long, (errorMsg, weatherResults) => {
            if (errorMsg) {
                console.log(errorMsg);
            } else {
                // console.log(JSON.stringify(weatherResults, undefined, 2));
                console.log(`Its currently ${weatherResults.temperature}. It feels like ${weatherResults.apparentTemperature}.`);
            }
        });
    }
});