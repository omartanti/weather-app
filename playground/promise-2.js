const request = require('request');

let geocodeAddress = (address) => {

    return new Promise((resolve, reject) => {
        let addressEncoded = encodeURIComponent(address);
        request({
            url: `http://www.mapquestapi.com/geocoding/v1/address?key=qqgefncuq0uIRtBxINoPgRhHEX2bhfH2&location=${addressEncoded}`,
            json: true
        }, (error, response, body) => {

            if (error || body.info.statuscode !== 0) {
                reject('Could not find address location');
            } else {
                resolve({
                    street: body.results[0].locations[0].street,
                    county: body.results[0].locations[0].adminArea4,
                    city: body.results[0].locations[0].adminArea5,
                    country: body.results[0].locations[0].adminArea1,
                    lat: body.results[0].locations[0].latLng.lat,
                    long: body.results[0].locations[0].latLng.lng,
                });
            }

        });
    });

};

geocodeAddress('19146').then((location) => {
    console.log(JSON.stringify(location, undefined, 2));
}, (errorMsg) => {
    console.log(errorMsg);
});