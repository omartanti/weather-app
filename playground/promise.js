let asyncAdd = (a, b) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (typeof a === 'number' && typeof b === 'number') {
                resolve(a + b);
            } else {
                reject('Arguments Must be numbers');
            }
        }, 1500);
    });
};

asyncAdd(5, 7).then((response) => {
    console.log('Result: ', response);
    return asyncAdd(response, 33);
}).then((response2) => {
    console.log('Result: ', response2);
}).catch( (errorMsg) => {
    console.log(errorMsg);
});

let somePromise = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve('HEY. It worked');
        // reject('Error: Unable to fulfill promise');
    }, 2500);
});

somePromise.then((message) => {
    console.log(`Success:  ${message}`);
}, (errorMsg) => {
    console.log(errorMsg);
});