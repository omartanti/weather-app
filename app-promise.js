const yargs = require('yargs');
const axios = require('axios');

const argv = yargs
    .options({
        address: {
            alias: 'a',
            demand: true,
            describe: 'Address to fetch weather for',
            string: true,
        }
    })
    .help()
    .alias('help', 'h') //this creates an alias for help as 'h'
    .argv;

let addressEncoded = encodeURIComponent(argv.address);
let geocodeUrl = `http://www.mapquestapi.com/geocoding/v1/address?key=qqgefncuq0uIRtBxINoPgRhHEX2bhfH2&location=${addressEncoded}`;

axios.get(geocodeUrl).then((response) => {

    if(response.data.info.statuscode !== 0){
        throw new Error('Unable to find the address');
    }

    let lat = response.data.results[0].locations[0].latLng.lat;
    let long = response.data.results[0].locations[0].latLng.lng;
    let weatherUrl = `https://api.darksky.net/forecast/efb16dde4e0805c2ff6c44b6dc417e38/${lat},${long}`;

    console.log(`Street: ${response.data.results[0].locations[0].street}`);
    console.log(`County: ${response.data.results[0].locations[0].adminArea4}`);
    console.log(`City: ${response.data.results[0].locations[0].adminArea5}`);
    console.log(`Country: ${response.data.results[0].locations[0].adminArea1}`);

    return axios.get(weatherUrl);

}).then((response) => {

    let temperature = response.data.currently.temperature;
    let apparentTemperature = response.data.currently.apparentTemperature;

    console.log(`Its currently ${temperature}. It feels like ${apparentTemperature}.`);

}).catch((e) => {

    if (e.code === 'ENOTFOUND') {
        console.log('Unable to connect to API Servers');
    }else{
        console.log(e.message);
    }

});